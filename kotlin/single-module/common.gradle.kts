val scriptLocation = System.getenv("GRADLE_SCRIPTS") ?: project.properties["scriptLocation"]
apply("${scriptLocation}/kotlin/repositories.gradle.kts")
apply("${scriptLocation}/kotlin/logger.gradle.kts")
apply("${scriptLocation}/kotlin/common.gradle.kts")
apply("${scriptLocation}/kotlin/apply-tokens.gradle.kts")
apply("${scriptLocation}/kotlin/publish/repositories.gradle.kts")
