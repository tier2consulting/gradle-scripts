repositories {
    // Always look in the local Maven repository first
    mavenLocal()
    // Check the Tier 2 repository which contains mirrors for most of the other repositories we"re likely to need
    maven {
        url = uri("https://repository.tier2consulting.uk/repository/maven-internal")
        credentials {
            username = System.getenv("REGISTRY_USERNAME") ?: project.properties["repositoryUsername"] as String? ?: ""
            password = System.getenv("REGISTRY_PASSWORD") ?: project.properties["repositoryPassword"] as String? ?: ""
        }
    }
    // Search any proxied repositories via the Tier 2 repository
    maven {
        url = uri("https://repository.tier2consulting.uk/repository/maven-public")
        credentials {
            username = System.getenv("REGISTRY_USERNAME") ?: project.properties["repositoryUsername"] as String? ?: ""
            password = System.getenv("REGISTRY_PASSWORD") ?: project.properties["repositoryPassword"] as String? ?: ""
        }
    }
    // Check Google and Maven Central for any outstanding artifacts (this is only useful if repository.tier2consulting.com is down for any reason)
    google()
    mavenCentral()
}

