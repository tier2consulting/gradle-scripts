apply(plugin = "jacoco")
apply(plugin = "org.sonarqube")

allprojects {

    var exclusions = mutableListOf(
        "*Repository.class",
        "*Builder.class",
        "Beans.class",
        "Config.class",
        "CorsConfig.class",
        "SecurityConfig.class",
    )
    if (project.hasProperty("coverageExclusions")) {
        exclusions.addAll((project.properties["coverageExclusions"] as String).split(","))
    }
    tasks.withType<Test> {
        with(tasks.named("jacocoTestReport")) {
            enabled = true
            excludes = exclusions
        }
    }
    tasks.withType<JacocoTestReport> {
        reports {
            xml.required = true
            csv.required = false
            html.required = false
        }
    }
}

val coverageReports = listOf("${rootDir}/build/reports/jacoco/test/jacocoFullReport.xml")
if (project.hasProperty("includedInCoverage")) {
    project.properties["includedInCoverage"].split(",").each {
        p -> coverageReports.add("${p}/build/reports/jacoco/test/jacocoFullReport.xml")
    }
}

tasks.withType<Sonar> {
    properties {
        property("sonar.projectKey", project.properties["sonarProjectKey"])
        property("sonar.projectName", rootProject.name)
        property("sonar.host.url", project.properties["sonarHost"])
        property("sonar.token", project.properties["sonarToken"])
        property("sonar.gradle.skipCompile", true)

        println("Including reports: ${coverageReports}")
        property("sonar.coverage.jacoco.xmlReportPaths", coverageReports)
    }
}
