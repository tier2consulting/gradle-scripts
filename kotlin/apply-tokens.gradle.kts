import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

// Helper function added to "extra" that automatically applies default tokens into various properties files
val applyTokens by extra(
    fun(proj: Project, tokens: Map<String, Any?>): String {
        val formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")

        val t = mutableMapOf(
            "projectName" to proj.name,
            "group" to proj.properties["group"],
            "artifactName" to proj.properties["artifactName"],
            "applicationVersion" to proj.properties["version"],
            "buildTime" to LocalDateTime.now().format(formatter),
            "logLevel" to (project.properties["log.level"] ?: project.properties[if (project.properties["isSnapshot"] as Boolean) "testLogLevel" else "prodLogLevel"])
        )
        t.putAll(tokens)

        extra["tokens"] = t
        return project.name
    }
)
