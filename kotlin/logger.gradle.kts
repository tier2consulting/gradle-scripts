import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import org.gradle.api.Project
import org.gradle.BuildAdapter
import org.gradle.BuildResult
import org.gradle.api.logging.Logger

class BuildInfo : BuildAdapter {

    val logger: Logger
    val project: Project
    val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")

    constructor(logger: Logger, project: Project, isSnapshot: Boolean = false) {
        this.logger = logger
        this.project = project

        logger.lifecycle("******************************************************************************")
        logger.lifecycle(" Building: ${project.properties["projectName"]}")
        logger.lifecycle("")
        if (project.hasProperty("group"))               logger.lifecycle(" Group:       ${project.properties["group"]}")
        if (project.hasProperty("artifactName"))        logger.lifecycle(" Artifact:    ${project.properties["artifactName"]}")
        if (project.hasProperty("version"))             logger.lifecycle(" Version:     ${project.properties["version"]}")
        if (project.hasProperty("classifier"))          logger.lifecycle(" Classifier: ${project.properties["classifier"]}")
        if (project.hasProperty("packaging"))           logger.lifecycle(" Packaging:   ${project.properties["packaging"]} ")
        if (project.hasProperty("artifactDescription")) logger.lifecycle(" Description: ${project.properties["artifactDescription"]}")

        if (isSnapshot) {
            logger.lifecycle("")
            logger.lifecycle("             *** WARNING: Project is in SNAPSHOT mode! ***")
        }
        logger.lifecycle("")
        logger.lifecycle(" Build time: ${LocalDateTime.now().format(formatter)}")
        logger.lifecycle("******************************************************************************")
    }

    override fun buildFinished(result: BuildResult) {
        logger.lifecycle("\n** Completed: ${project.properties["projectName"]} :: ${project.properties["version"]} at: ${LocalDateTime.now().format(formatter)}")
    }
}
extra["isSnapshot"] = (project.properties["version"] as String).endsWith("-SNAPSHOT")
gradle.addBuildListener(BuildInfo(logger, project, extra["isSnapshot"] as Boolean))
