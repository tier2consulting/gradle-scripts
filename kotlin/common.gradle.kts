import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.tasks.bundling.Jar

project.extra["isSnapshot"] = (version as String).endsWith("-SNAPSHOT", true)

allprojects {

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<ProcessResources> {
        from("${project.projectDir}/src/main/resource")
        filesMatching("*.(cfg|json|properties|txt|xml|yml|yaml)") {
            expand(extra["tokens"] as Map<String, Any?>)
        }
    }

    tasks.withType<Javadoc> {
        setFailOnError(false)// = false
    }

    tasks.withType<Test> {
        if (project.hasProperty("runOnly.integration-tests")) {
            logger.lifecycle("Running only integration tests...")
            exclude("**/*Test.class")
            include("**/*IT.class")
        } else if (!project.hasProperty("exclude.integration-tests")) {
            exclude("**/*IT.class")
        } else {
            logger.lifecycle("Including integration tests...")
        }

        doFirst {
            logger.lifecycle("\nRunning unit tests...")
        }
        doLast {
            logger.lifecycle("Finished running unit tests!\n")
        }
        afterTest(KotlinClosure2({ desc: TestDescriptor, result: TestResult ->
            logger.lifecycle(
                "Executed: [${desc.className}${if (desc.name != "test")" > ${desc.name} " else ""}] " +
                "Result: ${result.resultType} ${if (result.resultType == TestResult.ResultType.FAILURE) "(tests failed: ${result.failedTestCount}/${result.testCount})" else ""}")
        }))
    }
}
