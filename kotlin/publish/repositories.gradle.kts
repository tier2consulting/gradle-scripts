configure<PublishingExtension> {
    repositories {
        maven {
            val repoHost = System.getenv("REGISTRY_HOST") ?: project.properties["repositoryHost"]
            var repoName: String?
            if (extra["isSnapshot"] as Boolean) {
                repoName = System.getenv("REGISTRY_SNAPSHOTS_LOCATION") ?: project.properties["snapshotRepo"] as String? ?: ""
            } else {
                repoName = System.getenv("REGISTRY_RELEASES_LOCATION") ?: project.properties["releaseRepo"] as String? ?: ""
            }

            url = uri(if (repoName.startsWith("https")) repoName else "$repoHost/repository/$repoName")
            credentials {
                username = System.getenv("REGISTRY_USERNAME") ?: project.properties["nexusUsername"] as String? ?: ""
                password = System.getenv("REGISTRY_PASSWORD") ?: project.properties["nexusPassword"] as String? ?: ""
            }
        }
    }
}
