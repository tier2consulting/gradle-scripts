configure<PublishingExtension> {
    publications {
        create<MavenPublication>("Maven") {
            groupId = project.properties["group"] as String
            artifactId = project.properties["artifactName"] as String
            artifact(tasks.getByName("bootJar"))
        }
    }
}
