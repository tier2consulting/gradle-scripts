import java.io.File
import java.util.Properties
import java.io.FileInputStream

fun removeSnapshot(): String {
    val ver: String = "$version"
    return if ("$ver".endsWith("-SNAPSHOT", true)) "$version".substring(0, "$ver".length - 9)
    else ver
}

fun generateVersion(properties: Properties): String {
    val ver: String = "$version"
    val updateMode = properties["mode"] ?: "patch"
    val (oldMajor, oldMinor, oldPatch) = ver.split(".").map(String::toInt)
    var (newMajor, newMinor, newPatch) = arrayOf(oldMajor, oldMinor, 0)
    when (updateMode) {
        "major" -> newMajor = (oldMajor + 1).also { newMinor = 0 }
        "minor" -> newMinor = oldMinor + 1
        else -> newPatch = oldPatch + 1
    }
    return "$newMajor.$newMinor.$newPatch"
}

fun loadProperties(propertyFile: File): Properties {
    return Properties().apply {
        load(FileInputStream(propertyFile))
    }
}

fun collectPropertiesAndOverrideVersion(props: Properties, newVersion: String): LinkedHashMap<String, Any> {
    val newProps = LinkedHashMap<String, Any>()
    props.forEach { prop ->
        logger.debug("Property: ${prop.key}=${prop.value}")
        if (prop.key == "version") {
            logger.info("Writing version=$newVersion to gradle.properties")
            newProps["version"] = newVersion
        } else {
            logger.info("Writing ${prop.key}=${prop.value} to gradle.properties")
            newProps[prop.key as String] = prop.value
        }

    }
    return newProps
}

tasks.register<WriteProperties>("unsnapshot") {

    val propertyFile = project.file("gradle.properties")

    group = "release tasks"
    destinationFile = propertyFile
    description = "Increments the version in this build file everywhere it is used."

    val newVersion = project.properties["overrideVersion"] as String? ?: removeSnapshot()
    logger.lifecycle("Setting release version: $newVersion")

    val props = loadProperties(propertyFile)
    properties(collectPropertiesAndOverrideVersion(props, newVersion))
}

tasks.register<WriteProperties>("incrementVersion") {

    val propertyFile = project.file("gradle.properties")

    group = "release tasks"
    destinationFile = propertyFile
    description = "Increments the version in this build file everywhere it is used."

    val props = loadProperties(propertyFile)
    val newVersion = generateVersion(props) + "-SNAPSHOT"
    logger.lifecycle("Setting new version: $newVersion")

    properties(collectPropertiesAndOverrideVersion(props, newVersion))
}

tasks.register("printVersion") {
    println("$version")
}

tasks.register("printArtifact") {
    println("${project.properties["artifactName"]}")
}
